<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css">
</head>
<body>


<div class="service-one-video" id="serviceOneVideo">
    <div class="service-one-video-nav">
        <button id="call"><i class="fa fa-phone" aria-hidden="true"></i></button>
        <button id="answer" style="display: none"><i class="fa fa-phone" aria-hidden="true"></i></button>
        <button id="hangup" style="display: none"><i class="fa fa-times" aria-hidden="true"></i></button>
        <button id="remoteFullscreen" style="display: none"><i class="fa fa-expand" aria-hidden="true"></i></button>
    </div>
    <video id="localVideo" data-id="2" data-url="/" autoplay muted playsinline></video>
    <video id="remoteVideo" data-id="2" data-url="/" height="378" autoplay playsinline></video>
</div>
<audio src="/audio/incoming-call-sound.mp3" id="incomingSound" style="display: none" loop></audio>
<audio src="/audio/outcoming-call-sound.mp3" id="outcomingSound" style="display: none" loop></audio>
<audio src="/audio/hangup-sound.mp3" id="hangupSound" style="display: none"></audio>



<!-- Modal -->
<div class="modal fade" id="noAudioVideo" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Помилка камери</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Помилка камери
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="node_modules/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/6.4.0/adapter.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/peerjs/0.3.16/peer.min.js"></script>

<script>
    var localUser = '2';
    var remoteUser = '1';
    var offlineMessage = '1 is offline';
    var cameraErrorMessage = 'camera error';
    var titleNotification = 'title notification';
    var duplicatedTabErrorMessage = 'duplicatedTabErrorMessage';
    var profileUrl = '/2.php';
</script>
<script src="dist/main.js"></script>
</body>
</html>