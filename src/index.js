'use strict';

var config = require('../PTPServer-config.json');

var localVideoStream;
var callObject;
var connected;
var socket;
var peer;
var recorder;
var checkForDisconnectState = false;
var checkForAvailableState = false;
var checkForHangupState = false;
var checkForStopRecord = false;
var checkForVideoName = false;
var incomingCallSound = document.getElementById('incomingSound');
var outcomingCallSound = document.getElementById('outcomingSound');
var hangupSound = document.getElementById('hangupSound');
window.incomingCallEvent = new Event('incomingCall');
window.outcomingCallEvent = new Event('outcomingCall');
window.callEndEvent = new Event('callEnd');
window.hangupEvent = new Event('hangup');
window.callStartEvent = new Event('callStart');
window.companionOfflineEvent = new Event('companionOffline');
window.duplicatedTabEvent = new Event('duplicatedTab');
 
var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');
var bookServiceId = document.querySelector('#localVideo').getAttribute('data-id');
var saveUrl = document.querySelector('#localVideo').getAttribute('data-url');

$('#remoteFullscreen').click(function () {
    var elem = document.getElementById("remoteVideo");
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }
});

startSocket([checkForOpenTab]);

// var peer = new Peer(localUser,
//     {
//         config: {
//             'iceServers': [
//                 {'url': config["video.iceServers.url"]}
//             ]
//         },
//         host: config["video.host"],
//         port: config["video.port"],
//         path: config["video.path"]
//     }); //connection for a/v stream

function checkForOpenTab() {
    socket.emit('checkOpenTab', localUser);
    socket.on('status', function(status) {
        if (status === 'opened') {
            document.dispatchEvent(window.duplicatedTabEvent);
        } else if (status === 'closed') {
            peer = new Peer(localUser,
                {
                    config: {
                        'iceServers': [
                            {'url': config["video.iceServers.url"]}
                        ]
                    },
                    host: config["video.host"],
                    port: config["video.port"],
                    path: config["video.path"]
                }); //connection for a/v stream

            listenForCall(peer);
        }
    });
}

// call side start

function call() {
    startSocket([checkAvailable, listenForRequests, checkForDisconnect]);
}

function callProcess() {
    if (!callObject) {
        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: true
        })
            .then(createLocalStream)
            .then(peerCall)
            .then(listenForStreamWithFeedback)
            .then(function () {
                document.dispatchEvent(window.outcomingCallEvent);
            })
            .then(listenForClose).catch(function(e) {
            $.notify({
                // options
                title: titleNotification,
                message: cameraErrorMessage
            }, {
                // settings
                element: 'body',
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                z_index: 99999,
                template: '<div class="notification-wind"><div class="notification-wind-pic"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="notification-wind-body"><div class="notification-wind-title">{1}</div><div class="notification-wind-description">{2}</div></div></div>'
            });

            console.log(e);
        });
    }
}

function peerCall() {
    callObject = peer.call(remoteUser, localVideoStream);
}

function listenForStreamWithFeedback() {
    callObject.on('stream', function(stream) {
        remoteVideo.srcObject = stream;
        callObject.answer(localVideoStream);
        connected = true;
        document.dispatchEvent(window.callStartEvent);
    })
}

$('#call').click(function() {
    call();
});

//call side end

//answer side start

function listenForCall(peer) {
    peer.on('call', function(call) {
        startSocket([listenForRequests, checkForDisconnect]);
        if (!callObject) callObject = call;
        document.dispatchEvent(window.incomingCallEvent);
        // someone calling message user calling you
    });
}

// peer.on('call', function(call) {
//     startSocket([listenForRequests, checkForDisconnect]);
//     if (!callObject) callObject = call;
//     document.dispatchEvent(window.incomingCallEvent);
//     // someone calling message user calling you
// });

$('#answer').click(function() {
    $('#hangup').attr('disabled', 'disabled');
    $('#answer').attr('disabled', 'disabled');
    if (callObject && !connected) {
        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: true
        }).then(createLocalStream)
            .then(answerForStream).then(listenForStream).then(listenForClose).catch(function(e) {
            $("#noAudioVideo").modal('show');

            $.notify({
                // options
                title: titleNotification,
                message: cameraErrorMessage
            }, {
                // settings
                element: 'body',
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                z_index: 99999,
                template: '<div class="notification-wind"><div class="notification-wind-pic"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="notification-wind-body"><div class="notification-wind-title">{1}</div><div class="notification-wind-description">{2}</div></div></div>'
            });

            console.log(e);
        });
    }
});

function answerForStream() {
    callObject.answer(localVideoStream);
    connected = true;
}

function listenForStream() {
    callObject.on('stream', function(stream) {
        remoteVideo.srcObject = stream;
        document.dispatchEvent(callStartEvent);
        $('#hangup').removeAttr('disabled');
        $('#answer').removeAttr('disabled');

    });
}

//answer side end

//both sides start

function createLocalStream(stream) {
    localVideoStream = stream;
    localVideo.srcObject = stream;
}

function listenForClose() {
    callObject.on('close', function() {
        localVideo.srcObject = null;
        remoteVideo.srcObject = null;
        callObject = null;
        connected = false;
        for (var i = 0; i < localVideoStream.getTracks().length; i ++) {
            localVideoStream.getTracks()[i].stop();
        }
        document.dispatchEvent(window.callEndEvent);
    })
}

function hangup() {
    callObject.close();
}

function localHangup() {
    localVideo.srcObject = null;
    remoteVideo.srcObject = null;
    callObject = null;
    connected = false;
    if (localVideoStream) {
        for (var i = 0; i < localVideoStream.getTracks().length; i ++) {
            localVideoStream.getTracks()[i].stop();
        }

        outcomingCallSound.pause();
        outcomingCallSound.currentTime = 0;
        incomingCallSound.pause();
        incomingCallSound.currentTime = 0;
        hangupSound.currentTime = 0;
        hangupSound.play();
    }
}

$('#hangup').click(function() {
    if (callObject && callObject.open) {
        hangup();
    } else if (callObject && !callObject.open) {
        localHangup();
        remoteHangup();
    }else {
        localHangup();
    }
    document.dispatchEvent(window.hangupEvent);
});

window.onbeforeunload = function(){
    if (callObject && callObject.open) {
        hangup();
    } else if (callObject && !callObject.open) {
        localHangup();
        remoteHangup();
    }
};

//both sides end

//socket start

function startSocket(callbacks) {
    if (!socket) {
        socket = io.connect(config['video.io.connect']);
    } //create socket connection
    if (callbacks) {
        for (var i = 0; i < callbacks.length; i++) {
            callbacks[i]();
        }
    }
}

function checkForDisconnect() {
    if (checkForDisconnectState === false) {
        socket.on('disconnect', function() {
            socket.open();
        });
        checkForDisconnectState = true;
    }
}

function checkAvailable() {
    socket.emit('checkAvailable', remoteUser);
    if (checkForAvailableState === false) {
        socket.on('available', function(status) {
            if (status === true) {
                callProcess();
            } else {
                document.dispatchEvent(window.companionOfflineEvent);
            }
        });
        checkForAvailableState = true;
    }
}

function remoteHangup() {
    socket.emit('hangup', remoteUser);
}

function listenForRequests() {
    if (checkForHangupState === false) {
        socket.on('hangup', function(user) {
            if (user === localUser) {
                remoteVideo.poster = remoteVideo.poster;
                localHangup();
                outcomingCallSound.pause();
                outcomingCallSound.currentTime = 0;
                incomingCallSound.pause();
                incomingCallSound.currentTime = 0;
                document.dispatchEvent(window.callEndEvent);
            }
        });
        checkForHangupState = true;
    }

}

//socket end


//companion offline event
document.addEventListener('companionOffline', function () {
    if ($('body').find('.alert').last().length) {
        $('body').find('.alert').last().remove();
    }
    $.notify({
        // options
        title: titleNotification,
        message: offlineMessage
    }, {
        // settings
        element: 'body',
        newest_on_top: true,
        type: 'success',
        allow_dismiss: true,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        z_index: 99999,
        template: '<div class="notification-wind"><div class="notification-wind-pic"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="notification-wind-body"><div class="notification-wind-title">{1}</div><div class="notification-wind-description">{2}</div></div></div>'
    });
});

//incoming call event
document.addEventListener('incomingCall', function () {
    incomingCallSound.currentTime = 0;
    incomingCallSound.play();
    outcomingCallSound.pause();
    outcomingCallSound.currentTime = 0;
    hangupSound.pause();
    hangupSound.currentTime = 0;
    $('#call').hide();
    $('#answer').show();
    $('#hangup').show();
    $('#remoteFullscreen').hide();
});

//outcoming call event
document.addEventListener('outcomingCall', function () {
    outcomingCallSound.currentTime = 0;
    outcomingCallSound.play();
    incomingCallSound.pause();
    incomingCallSound.currentTime = 0;
    hangupSound.pause();
    hangupSound.currentTime = 0;
    $('#call').hide();
    $('#answer').hide();
    $('#hangup').show();
    $('#remoteFullscreen').hide();
});

//outcoming call event
document.addEventListener('callStart', function () {
    outcomingCallSound.pause();
    outcomingCallSound.currentTime = 0;
    incomingCallSound.pause();
    incomingCallSound.currentTime = 0;
    hangupSound.pause();
    hangupSound.currentTime = 0;
    $('#serviceOneVideo').addClass('call-start');
    $('#answer').hide();
    $('#remoteFullscreen').show();
    $('#hangup').show();

    record();
});

//companion hangup event
document.addEventListener('hangup', function () {
    remoteVideo.poster = remoteVideo.poster;
    outcomingCallSound.pause();
    outcomingCallSound.currentTime = 0;
    incomingCallSound.pause();
    incomingCallSound.currentTime = 0;
    hangupSound.pause();
    hangupSound.currentTime = 0;
    $('#serviceOneVideo').removeClass('call-start');
    $('#call').show();
    $('#hangup').hide();
    $('#answer').hide();
    $('#remoteFullscreen').hide();

    stopRecording();
});

//end of call event
document.addEventListener('callEnd', function () {
    $('#serviceOneVideo').removeClass('call-start');
    $('#call').show();
    $('#hangup').hide();
    $('#answer').hide();
    $('#remoteFullscreen').hide();

    stopRecording();
});

document.addEventListener('duplicatedTab', function () {
    $.notify({
        // options
        title: titleNotification,
        message: duplicatedTabErrorMessage
    }, {
        // settings
        element: 'body',
        newest_on_top: true,
        type: 'success',
        allow_dismiss: true,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        z_index: 99999,
        template: '<div class="notification-wind"><div class="notification-wind-pic"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="notification-wind-body"><div class="notification-wind-title">{1}</div><div class="notification-wind-description">{2}</div></div></div>'
    });
    $('.main').remove();
    setTimeout(function () {
        location.href = profileUrl;
    }, 1000);
});

function record() {
    recorder = new MediaRecorder(localVideoStream, {
        audioBitsPerSecond: config["video.audioBitsPerSecond"],
        videoBitsPerSecond: config["video.videoBitsPerSecond"],
        mimeType: config["video.mimeType"]
    });
    recorder.onstart = function() {
        socket.emit('videoRecordStart', localUser);
    };
    recorder.onstop = function() {
        socket.emit('videoRecordEnd', {user: localUser, service: bookServiceId}); 
        
        console.log(1, '../records/' + bookServiceId+"/" + localUser + '_somedate.webm');

        if (checkForVideoName === false) {
            socket.on('name', function(name) {
                if (name != "") {
                    $.ajax({
                        url: saveUrl,
                        type: 'POST',
                        data: {
                            'name': name,
                            'bookServiceId': bookServiceId
                        },
                        success: function(res) {
                            console.log(2, '../records/' + bookServiceId + "/" + localUser + '_somedate.webm' , name);
                        },
                        error: function(response) {
                            console.log(response);
                        }
                    });
                }
            });
            checkForVideoName = true;
        }
    };
    recorder.ondataavailable = function(event) {
        if (event.data.size > 0) {
            socket.emit('video', {'user': localUser, 'part': event.data});
        }
    };
    recorder.start(1000);
}

function stopRecording() {
    if (recorder && recorder.state === 'recording') {
        recorder.stop();
    }
}