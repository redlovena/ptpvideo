'use strict';

var config = require('../PTPServer-config.json');

var fs = require('fs');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var https = require('https');
var express = require('express');
var socketIO = require('socket.io');
var PeerServer = require('peer').PeerServer;
var peerConnectionList = [];
var ioConnectionList = [];
var socketToUserRelation = [];
var videoFileParts = [];

var writeFileSync = function (path, bookService, file, buffer, permission) {
    permission = permission || 438; // 0666
    var fileDescriptor;

    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }

    if (!fs.existsSync(path + bookService)) {
        fs.mkdirSync(path + bookService);
    }

    try {
        fileDescriptor = fs.openSync(path + bookService + file, 'w', permission);
    } catch (e) {
        fs.chmodSync(path, permission);
        fileDescriptor = fs.openSync(path + bookService + file, 'w', permission);
    }

    if (fileDescriptor) {
        fs.writeSync(fileDescriptor, buffer, 0, buffer.length, 0);
        fs.closeSync(fileDescriptor);
    }
};
var options = {
    key: fs.readFileSync(appDir + config["app.ssl.key"]),
    cert: fs.readFileSync(appDir + config["app.ssl.crt"])
};

var server = PeerServer(
    {
        port: config["video.port"],
        path: config["video.path"],
        ssl: options
    }
);

server.on('connection', function(id) {
    if (typeof peerConnectionList[id] === 'undefined') {
        peerConnectionList[id] = id;
    }
});

server.on('disconnect', function(id) {
    delete peerConnectionList[id];
});

var app = express();
var httpsServer = https.createServer(options, app).listen(config["app.listen.port"]);

var io = socketIO.listen(httpsServer);

io.sockets.on('connection', function(socket) {
    socket.on('checkAvailable', function(user) {
        if (peerConnectionList.indexOf(user) !== -1) {
            socket.emit('available', true);
        } else {
            socket.emit('available', false);
        }
    });

    socket.on('checkOpenTab', function(user) {
        if (peerConnectionList.indexOf(user) !== -1) {
            socket.emit('status', 'opened');
        } else {
            socketToUserRelation[socket.id] = user;
            ioConnectionList[user] = socket;
            socket.emit('status', 'closed');
        }
    });

    socket.on('hangup', function(user) {
        if (typeof ioConnectionList[user] !== 'undefined') {
            ioConnectionList[user].emit('hangup', user);
        }
    });

    socket.on('disconnect', function (socket) {
        if (typeof socketToUserRelation[socket.id] !== 'undefined') {
            delete ioConnectionList[socketToUserRelation[socket.id]];
            delete socketToUserRelation[socket.id];
        }
    });

    socket.on('videoRecordStart', function(user) {
        videoFileParts[user] = [];
    });

    socket.on('videoRecordEnd', function (data) { 
        var curdate = Date.now();
        var name = data.user +'_' + curdate + '.webm';
        var fullBuffer = Buffer.concat(videoFileParts[data.user]);
        writeFileSync('../records/', data.service+"/", name, fullBuffer, '0755');
        videoFileParts[data.user] = [];
        socket.emit('name', name);
    });

    socket.on('video', function(data) {
        videoFileParts[data.user].push(data.part);
    });

});